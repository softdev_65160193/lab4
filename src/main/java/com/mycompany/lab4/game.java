/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author waran
 */
import java.util.Scanner;
public class game {
   private player Player1;
    private player Player2;
    private board board;
    private player currentPlayer;

    public game() {
        Player1 = new player('X');
        Player2 = new player('O');
        currentPlayer = Player1;
        board = new board();
    }
    
    public boolean checkWin(){
        for(int i = 0; i < 3; i++ ){
        if(board.getBoard(i,0)==board.getBoard(i,1)&&board.getBoard(i,1)==board.getBoard(i,2)){
            return true;
        }
        else if(board.getBoard(0,i)==board.getBoard(1,i)&&board.getBoard(1,i)==board.getBoard(2,i)){
            return true;
        }
        }
         if(board.getBoard(0,0)==board.getBoard(1,1)&&board.getBoard(1,1)==board.getBoard(2,2)){
            return true;
         }
         else if(board.getBoard(0,2)==board.getBoard(1,1)&&board.getBoard(1,1)==board.getBoard(2,0)){
            return true;
         }
         return false;
    }
       
    public boolean checkTie(){
        for(int row = 0; row < 3; row++){
            for(int col = 0; col < 3; col++){
                if(Character.isDigit(board.getBoard(row, col))){
                    return false;
                }
            }
         }
        return true;
    }
    
    public void placePiece(){
     Scanner kb = new Scanner(System.in);
        System.out.println(currentPlayer.getSymbol()+" turn");
        System.out.print("Please input number : ");
        
        int place;
        do {
            place = kb.nextInt();
            
        } while (!isValidMove(place));
            int row = (place - 1) / 3;
            int col = (place - 1) % 3;
            board.setBoard(row, col, currentPlayer.getSymbol());
        

        }
        
     
    
    private boolean isValidMove(int place) {
        int row = (place - 1) / 3;
        int col = (place - 1) % 3;
        return Character.isDigit(board.getBoard(row, col));
    }
    
    public boolean playAgain(){
        Scanner kb = new Scanner(System.in);
        System.out.println("Do you want to continue? {Y/N}");
        String choice = kb.next().trim().toLowerCase();
        if(choice.equals("y")){
           return true;
        }
        return false;
    }
    public void printWelcome(){
        System.out.println("Welcome to Ox game");
    }
    
    
    public void switchPlayer(){
    if(currentPlayer==Player1){
        currentPlayer= Player2;
    }else if(currentPlayer==Player2){
        currentPlayer= Player1;
    }
    }
    
    public void printScore(){
        System.out.println("Player 1= "+Player1.getScore()+"Player 2= "+Player2.getScore());
    }
    public void printBoard(){
        for (int i = 0; i<3; i++){
            for (int j = 0; j<3; j++){
                System.out.print(board.getBoard(i, j)+" ");
            }
            System.out.println();
        }
    }
     public void play() {
        printWelcome();
        while (true) {
            printBoard();
            placePiece();

            if (checkWin()) {
                currentPlayer.addScore();
                printBoard();
                System.out.println(currentPlayer.getSymbol() + " wins!");
                printScore();

                if (playAgain()) {
                    board.resetBoard();
                    currentPlayer = Player1;
                } else {
                    break;
                }
            } else if (checkTie()) {
                printBoard();
                System.out.println("It's a tie!");
                printScore();

                if (playAgain()) {
                    board.resetBoard();
                    currentPlayer = Player1;
                } else {
                    break;
                }
            }
            switchPlayer();
        }
    }
    
    
    
    
    
}
